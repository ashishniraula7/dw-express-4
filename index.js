//npm i express
// make express application =>
//attached port =>

import express, { Router, json } from "express";
import { firstRouter } from "./src/routes/firstRouter.js";
import mongoose, { Schema, model } from "mongoose";
import connectMongodb from "./src/connectdb/connectMongodb.js";
import { User } from "./src/schema/model.js";
import userRouter from "./src/routes/userRouter.js";

// localhost:8000/products  methode post

let app = express();
app.use(json());

app.use("/products", firstRouter);
app.use("/users", userRouter);

connectMongodb();


app.listen(8000, () => {
  console.log(`express application is listening at port 8000`);
});
