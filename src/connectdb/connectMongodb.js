import mongoose from "mongoose";
let connectMongodb = () => {
  mongoose.connect("mongodb://0.0.0.0:27017/express-dw-4");
};

export default connectMongodb;
