import { Schema } from "mongoose";

let userSchema = Schema({
  name: {
    type: String,
  },
  age: {
    type: Number,
  },
  isMarried: {
    type: Boolean,
  },
});

export default userSchema;
